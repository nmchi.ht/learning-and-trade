import React from 'react'
import Image from 'next/image'
import '../../styles/services.scss'

const ImageOverlay = ({ color, content }) => (
    <div className="overlay" style={{ backgroundColor: color }}>
        <p className="w-2/5 pt-2 pl-2 bg-transparent">{content}</p>
    </div>
)

const ImageContainer = ({
    src,
    alt,
    width,
    height,
    imgContent,
    overlayColor,
    overlayContent,
    title
}) => (
    <div className="image-container">
        <div className='relative'>
            <Image src={src} alt={alt} width={width} height={height} />
            {imgContent && <div>{imgContent}</div>}
            <div className=''>
                {title && <div className='text-white overlay'>{title}</div>}
                {/* <ImageOverlay color={overlayColor} content={overlayContent} /> */}
            </div>
        </div>
        {/* <ImageOverlay color={overlayColor} content={overlayContent} /> */}
    </div>
)

const Services = () => {
    const images = [
        {
            src: '/images/services/ser1.png',
            alt: 'Description of the image',
            width: 500,
            height: 300,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Thiết kế ấn phẩm ',
        },
        {
            src: '/images/services/ser2.png',
            alt: 'Description of the image',
            width: 500,
            height: 300,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Bao bì vỏ hộp',
            title: 'Thiết kế ấn phẩm'
        },
        {
            src: '/images/services/ser3.png',
            alt: 'Description of the image',
            width: 500,
            height: 300,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Yaourt nhà làm',
        },
        {
            src: '/images/services/ser4.png',
            alt: 'Description of the image',
            width: 500,
            height: 300,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Laptop Dell',
        },
        {
            src: '/images/services/ser5.png',
            alt: 'Description of the image',
            width: 500,
            height: 290,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Thiết kế ấn phẩm ',
        },
        {
            src: '/images/services/ser6.png',
            alt: 'Description of the image',
            width: 500,
            height: 290,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Bao bì vỏ hộp',
        },
        {
            src: '/images/services/ser7.png',
            alt: 'Description of the image',
            width: 500,
            height: 290,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Yaourt nhà làm',
        },
        {
            src: '/images/services/ser8.png',
            alt: 'Description of the image',
            width: 500,
            height: 290,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Laptop Dell',
        },
        {
            src: '/images/services/ser9.png',
            alt: 'Description of the image',
            width: 500,
            height: 290,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Yaourt nhà làm',
        },
        {
            src: '/images/services/ser10.png',
            alt: 'Description of the image',
            width: 500,
            height: 290,
            overlayColor: 'RGB(246,147,28)',
            overlayContent: 'Yaourt nhà làm',
        },
    ]

    return (
        <div className="service-area relative">
            <div className='bg-ser'>
                <img src='/images/services/ser2-bg.png' />
            </div>
            <div className='container mx-auto mb-5 relative' style={{ zIndex: '1' }}>
                <h1 className='pl-5 text-text-h1 font-bold'>DỊCH VỤ</h1>
            </div>
        
            <div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-5 xl:grid-cols-5 relative" style={{ zIndex: '1' }}>
                {images.map((image) => (
                    <div key={image.id} className="col-span-1 p-1">
                        <ImageContainer {...image} />
                    </div>
                ))}
                <div class='btn-see-more'>
                    <a class='flex items-center justify-center text-white text-center'>ĐẾN VỚI CHÚNG TÔI</a>
                </div>
            </div>
            <div className='bg-ser2'>
                <img src='/images/services/ser-bg.png' />
            </div>
        </div>
    )
}

export default Services
