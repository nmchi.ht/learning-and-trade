'use client'
import React, { useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import Dropdown from './Dropdown'

const options = ['Sản phẩm', 'Sản phẩm 1', 'Sản phẩm 2']

const handleDropdownSelect = (selectedOption) => {
  console.log(`Selected option: ${selectedOption}`)
}

const Navbar = () => {
  return (
    <>
      <div className="navbar-area">
        <div className="container mx-auto">
          <div className="row status-bar">
            <div
              className="col-lg-4"
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '5px',
                color: '#fff',
              }}
            >
              <svg
                width="15"
                height="15"
                viewBox="0 0 4 5"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1.94999 0.0199585C1.94999 0.0199585 -1.99001 0.619958 1.94999 4.77996C5.89999 0.619958 1.94999 0.0199585 1.94999 0.0199585ZM1.94999 2.39996C1.61999 2.39996 1.35 2.12996 1.35 1.79996C1.35 1.46996 1.61999 1.19996 1.94999 1.19996C2.27999 1.19996 2.55 1.46996 2.55 1.79996C2.55 2.12996 2.27999 2.39996 1.94999 2.39996Z"
                  fill="white"
                />
              </svg>
              66/52 Phổ Quang, P.2, Q. Tân Bình, TP. Hồ Chí Minh
            </div>
            <div
              className="col-lg-4"
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '5px',
                color: '#fff',
              }}
            >
              <svg
                width="15"
                height="15"
                viewBox="0 0 8 7"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M5.95999 6.53998H2.45999C1.52999 6.53998 0.779999 5.78998 0.779999 4.85998V2.52998C0.779999 1.59998 1.52999 0.849976 2.45999 0.849976H5.95999C6.88999 0.849976 7.64001 1.59998 7.64001 2.52998V4.86998C7.63001 5.78998 6.87999 6.53998 5.95999 6.53998Z"
                  fill="white"
                />
                <path
                  d="M1.87 1.59995C1.87 1.87995 1.91001 3.70995 3.42001 4.98995C4.62001 6.00995 5.98001 6.03995 6.35001 6.03995C6.41001 6.01995 6.49 5.97995 6.53 5.88995C6.57 5.80995 6.56 5.73995 6.56 5.70995C6.54 5.54995 6.54 5.25995 6.56 4.80995C6.55 4.78995 6.53 4.71995 6.47 4.67995C6.4 4.62995 6.33 4.61995 6.31 4.61995C6.19 4.61995 6.05001 4.61995 5.89001 4.58995C5.70001 4.55995 5.53999 4.51995 5.39999 4.47995C5.37999 4.46995 5.31001 4.43995 5.23001 4.45995C5.14001 4.47995 5.09001 4.53995 5.07001 4.55995C4.89001 4.73995 4.71 4.91995 4.53 5.10995C4.28 4.97995 3.96999 4.78995 3.64999 4.48995C3.21999 4.08995 2.96001 3.67995 2.82001 3.38995C3.01001 3.19995 3.20001 3.00995 3.39001 2.81995C3.41001 2.79995 3.46 2.73995 3.47 2.64995C3.48 2.56995 3.45 2.51995 3.44 2.48995C3.4 2.38995 3.36 2.24995 3.34 2.08995C3.32 1.92995 3.32001 1.78995 3.32001 1.66995C3.32001 1.64995 3.33001 1.56995 3.29001 1.49995C3.23001 1.39995 3.14 1.36995 3.12 1.36995C2.86 1.37995 2.65 1.36995 2.5 1.35995C2.25 1.33995 2.08 1.30995 1.97 1.41995C1.89 1.47995 1.87 1.54995 1.87 1.59995Z"
                  fill="#BE9452"
                />
              </svg>
              (+84) 28 2240 6511
            </div>
            <div
              className="col-lg-4"
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '5px',
                color: '#fff',
              }}
            >
              <img src="/images/lang/vn.svg" />
              TIẾNG VIỆT |
              <img src="/images/lang/ame.svg" />
              ENGLISH
            </div>
          </div>
        </div>

        <div className="nav-content">
          <div className="flex flex-col bg-bg-header bg-cover bg-center bg-no-repeat w-full h-full object-cover relative nav-items">
            <div class="container mx-auto">
              <div class="row flex justify-between items-center text-white py-4 px-6">
                <div class="col-md-5 flex items-center">
                  <a
                    href="/"
                    class="text-white  hover:underline no-underline mr-4 py-2 px-2"
                  >
                    Home
                  </a>
                  <Dropdown options={options} onSelect={handleDropdownSelect} />
                  <Dropdown options={options} onSelect={handleDropdownSelect} />
                </div>

                <div class="col-md-2 flex items-center justify-center">
                  <img src="/images/header/logo.svg" alt="Logo" />
                </div>
                <div class="col-md-5 flex items-center justify-end space-x-7">
                  <a href="#" class="text-white hover:underline no-underline">
                    Về chúng tôi
                  </a>
                  <a href="#" class="text-white  hover:underline no-underline">
                    Tin tức
                  </a>
                  <a href="#" class="text-white  hover:underline no-underline">
                    Liên hệ
                  </a>
                </div>
              </div>
            </div>

            <div className="nav-item mx-auto">
              <h2 className="text-white uppercase text-text-h2 font-bold">
                Sản xuất bền vững
              </h2>
              <span className="text-center text-white uppercase text-text-h2 font-bold px-5">
                |
              </span>
              <h2 className="text-center text-white uppercase text-text-h2 font-bold">
                thân thiện môi trường
              </h2>
              <span className="text-center text-white uppercase text-text-h2 font-bold px-5">
                |
              </span>
              <h2 className="text-center text-white uppercase text-text-h2 font-bold">
                Tiết kiệm năng lượng
              </h2>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Navbar
