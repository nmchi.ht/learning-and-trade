import Image from "next/image";
import React from "react";

const History = () => {
    return (
        <div className="history-area px-[10px] md:pl-[2%] md:pr-0 lg:pl-[17%] text-white pb-[8%] pt-[3%]">
            <div className="flex history-head">
                <h1 className="bg-clip-text text-transparent bg-center pb-0 mb-0 lg:mr-4 leading-none">30</h1>
                <div className="text-head">
                    <div className="text-content">
                        <div className="content-left">
                            <h5 className="text-year">Năm</h5>
                            <p className="content1">ĐỂ PHÁT TRIỂN</p>
                            <p className="content2">ĐỂ TẬN TỤY</p>
                        </div>

                        <div className="w-2 h-14 bg-white lg:mx-8 mx-5"></div>

                        <div className="content-right">
                            <p className="content3">Quá trình hình thành</p>
                            <p className="content4">và phát triển</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="history-content md:grid md:grid-cols-2 md:gap-2">
                <div className="[&>div>p>span]:pr-2 [&>*>span]:text-black [&>*>p>span]:text-black [&>*>span]:font-bold [&>*>p>span]:font-bold">
                    <div className="">
                        <p>
                            <span>4-1998:</span>
                            Thành lập Trung tâm Dịch vụ Quảng cáo và tổ chức Hội chợ: chuyên<br></br>
                            cung cấp các dịch vụ thiết kế, in ấn, quảng cáo ngoài trời, tổ
                            chức hội chợ,<br></br> sự kiện, thực hiện các dịch truyền thông, sản xuất
                            TVCs
                        </p>
                    </div>
                    <div className="">
                        <span>4-2005:</span><br></br>
                        <p>
                            - Chuyển đổi Trung tâm thành Công ty, thành lập công ty TNHH Quảng
                            cáo và<br></br> Thương Mại Dương Phong chuyên cung cấp các dịch vụ thiết
                            kế, in ấn,<br></br> quảng cáo ngoài trời, sản xuất thiết bị quảng cáo
                        </p>
                        <p>
                            - Xây dựng xưởng cơ khí chuyên xây lắp dàn dựng bảng biển quảng
                            cáo tấm<br></br> lớn, sản xuất thiết bị quảng cáo
                        </p>
                    </div>
                    <div className="">
                        <p>
                            <span>2008:</span>
                            xây dựng nhà in Dương Phong với quy mô nhỏ 700m2 chuyên in ấn<br></br> phẩm
                            quảng cáo, lịch, sách
                        </p>
                    </div>
                    <div className="">
                        <p>
                            <span>2012:</span>
                            nâng cấp nhà in Dương Phong lên quy mô 6.000m2, bổ sung thêm các<br></br>
                            máy móc phục vụ ngành sản xuất bao bì giấy, thùng hộp carton
                        </p>
                    </div>
                    <div className="">
                        <p>
                            <span>2012-2024:</span>
                            liên tục nâng cấp nhà in, xưởng sản xuất thiết bị quảng cáo, trang<br></br>
                            bị máy móc hiện đại, hoàn thiện quy trình sản xuất đảm bảo tự động
                            hoá<br></br> chiếm 70% các công đoạn.
                        </p>
                    </div>
                </div>
                {/* content */}
                <div className="mt-4 p-0 md:m-0">
                    <Image
                        src="/images/history/history.png"
                        width={0}
                        height={0}
                        sizes="100vw"
                        className="w-full"
                        alt="Picture of the author"
                    />
                </div>
                {/* image */}
            </div>
        </div>
    );
};

export default History;