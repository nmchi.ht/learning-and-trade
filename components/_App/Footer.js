import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLocationDot,
  faPhone,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import Link from "next/link";
import Image from "next/image";
import "@/styles/components/Footer.scss";

const Footer = () => {
  return (
    <div className="footer__comp py-[10px] h-auto bg-black text-white flex flex-col justify-center px-[2vw] lg:px-[5vw]">
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 lg:gap-4">
        <div className="col-span-2">
          <h1 className=" uppercase text-xl lg:text-2xl mb-2">
            {`CÔNG TY TNHH Quảng CÁO VÀ THƯƠNG Mại`} <br />
            {` DƯƠNG PHONG (DPA)`}
          </h1>
          <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
              <p className="lg:col-span-2">
                <FontAwesomeIcon icon={faLocationDot} /> 66/52 Phổ Quang, P. 2,
                Q. Tân Bình, Tp.HCM
              </p>
              <p className="whitespace-nowrap">
                <FontAwesomeIcon icon={faPhone} /> 08. 38 44 1111
              </p>
              <p className="whitespace-nowrap">fax: 08. 3997 1009</p>
              <p className="whitespace-nowrap">
                <FontAwesomeIcon icon={faEnvelope} /> contact@dpa.vn
              </p>
            </div>
            <Link
              href={"/"}
              className="bg-white rounded-[30px] h-fit p-2 contact__btn w-fit"
            >
              <div className="flex justify-between items-center">
                <div className="image w-[50px] h-[50px] rounded-full bg-black bg-[url('/dpa-logo.png')] bg-contain bg-center bg-no-repeat mr-2"></div>
                <div className="flex-col items-end">
                  <p className="whitespace-nowrap text-black font-bold text-sm lg:text-lg m-0">
                    <FontAwesomeIcon
                      icon={faPhone}
                      className="text-[#F47B20]"
                    />
                    Gọi cho chúng tôi
                  </p>
                  <p className="whitespace-nowrap text-[#F47B20] font-bold text-lg lg:text-2xl m-0">{`(+84) 913 927 471`}</p>
                </div>
              </div>
            </Link>
          </div>
        </div>
        {/* information */}
        <div className="flex flex-col items-start [&>a]:text-sm [&>a]:font-normal lg:ml-12">
          <h1 className="uppercase text-xl xl:text-3xl font-bold">
            OUR MENU CATEGORIES
          </h1>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            TRANG CHỦ
          </Link>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            GIỚI THIỆU
          </Link>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            THÔNG TIN
          </Link>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            THƯ VIỆN
          </Link>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            HỖ TRỢ
          </Link>
        </div>
        {/* Footer menu */}
        <div className="flex flex-col [&>a]:text-sm [&>a]:font-normal lg:ml-12">
          <h1 className="uppercase text-xl xl:text-3xl font-bold whitespace-nowrap ">
            LAST EVENTS
          </h1>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            Christmas
          </Link>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            Black Friday
          </Link>
          <Link href={"/"} className="hover:text-[#FBB03F]">
            Sunday Special
          </Link>
          {/* holiday */}
        </div>
      </div>
      {/* footer top */}
      <div className="flex lg:flex-col items-baseline justify-between">
        <div className="md:flex mt-8 [&>*]:mr-8 [&>*>h3]:font-bold [&>*>p]:pb-3 [&>*>p]:font-[350] w-full md:justify-between">
          <div>
            <h3>NHÀ IN DƯƠNG PHONG:</h3>
            <p>
              7/9A, Ấp 1, xã Xuân Thới Thượng, <br /> Huyện Hóc Môn, Tp.HCM
            </p>
          </div>
          <div>
            <h3>XƯỞNG SẢN XUẤT KỸ THUẬT SỐ:</h3>
            <p>số 5 Phạm Ngũ Lão, Quận Gò Vấp, Tp.HCM</p>
          </div>
          <div>
            <h3>VPĐD TẠI HÀ NỘI:</h3>
            <p>R.707, 65 Văn Miếu, Đống Đa , Hà Nội</p>
          </div>
        </div>

        <div className="flex justify-end w-fit lg:w-full">
          <div className="flex flex-col lg:flex-row justify-between items-start lg:items-center [&>a]:mr-4 h-fit [&>*]:mb-2">
            <Link
              href={"/"}
              className="hover:text-[#FBB03F] ml-2 whitespace-nowrap"
            >
              NEW LETTER
            </Link>
            <Link
              href={"/"}
              className="font-bold text-black hover:text-[#fff] bg-[#FBB03F] hover:bg-[#AC835E] p-2 rounded-xl lg:rounded-3xl transition hover:transition hover:duration-500 duration-500"
            >
              Enter your <br className="sm:hidden" /> e-mail adress
            </Link>
            <Link
              href={"/"}
              className="font-bold text-black hover:text-[#fff]  bg-[#FBB03F] hover:bg-[#AC835E] p-2 rounded-xl lg:rounded-3xl transition hover:transition hover:duration-500 duration-500"
            >
              Subscribe
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
