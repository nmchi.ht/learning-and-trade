import ProductList from '@/components/Home/ProductList'
import Navbar from '@/components/_App/Navbar'
import '../styles/productList.css';
import '../styles/style.scss'
import "../styles/bootstrap.min.css";
import History from "@/components/_App/History";
import Partner from "@/components/_App/Partner";
import Services from "@/components/_App/Services";
export default function Home() {
    return (
        <>
            <Navbar />
            <History />
            <ProductList />
            <Services />
            <Partner />
        </>
    )
}
